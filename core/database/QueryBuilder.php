<?php
namespace Oxthorn\database;

use PDO;

class QueryBuilder
{
    protected $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function selectAll($table)
    {
        $statement = $this->pdo->prepare("select * from {$table}");
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_CLASS);
    }

    // Fetches the latest entries for each category in the provided table. 'id' is used instead of 'timestamp' to allow for older entries to be cycled to the front if needed.
    public function selectLatest($table)
    {
        $statement = $this->pdo->prepare("SELECT description, category FROM {$table} WHERE id IN (SELECT MAX(id) FROM {$table} GROUP BY category) ORDER BY id DESC");
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_CLASS);
    }
}

