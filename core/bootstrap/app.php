<?php
use Oxthorn\database\QueryBuilder;
use Oxthorn\database\Connection;
use Oxthorn\Router;
use Oxthorn\Request;


$app = [];

$app['config'] = require 'config.php';

require 'vendor/autoload.php';


$app['database'] = new QueryBuilder(
    Connection::make($app['config']['database'])
);

require Router::load('bootstrap/routes.php')
    ->direct(Request::uri());
