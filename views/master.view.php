<!DOCTYPE html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Alex Oxthorn</title>
        <meta name="description" content="The portfolio of Alex Oxthorn.">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="apple-touch-icon" href="favicon.png">
        <!-- Styles -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"  integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="public/css/app.css">
        <!-- Fonts -->
        <link   href = 'http://fonts.googleapis.com/css?family=Lora'                    rel = 'stylesheet'  type = 'text/css'       />
    </head>
    <body class="container-fluid">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <section id="section_0" class="row">
            <!-- Bio -->
            <?php require_once __DIR__."/components/bio.view.php"; ?>
            <!-- Social icons -->
            <?php require_once __DIR__."/components/social.view.php"; ?>
            <!-- Navigation -->
            <?php require_once __DIR__."/components/nav.view.php"; ?>
        </section>
        <section id="section_1" class="row">
            <!-- Projects -->
            <?php require_once __DIR__."/components/projects.view.php"; ?>
        </section>
        <section id="section_2" class="row">
            <!-- Skills -->
            <?php require_once __DIR__."/components/skills.view.php"; ?>
            <!-- Contact -->
            <?php require_once __DIR__."/components/contact.view.php"; ?>
        </section>
        <script src="public/js/app.js"></script>
    </body>
</html>