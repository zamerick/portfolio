<h2>Projects</h2>
<ul id="projects">
    <?php foreach ($projects as $project) { ?>
        <li class='project'>
            <img class="project__image" width="400" height="400" src="<?=$project->IMG ?>" class="attachment-post-thumbnail" alt="">
            <div class="project__overlay_hide">
                <h3><a target="_blank" class="project__link" href="<?=$project->URL ?>"><?=$project->NAME ?></a></h3>
                <p><?=$project->DESCRIPTION ?></p>
            </div>
        </li>
    <?php } ?>   
</ul>


