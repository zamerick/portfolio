<div id="contact">
   <h2>Contact</h2>
   <p>I'm available for fulltime and/or freelance opportunities, so drop me a line at:</p>
   <p class="center"><a id="contact__link" href="mailto:admin@alex-oxthorn.com">admin@alex-oxthorn.com</a></p>
</div>