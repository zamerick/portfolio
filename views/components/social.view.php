<!--
    Implementation of svg sprite sheet from the below article:
    https://css-tricks.com/svg-sprites-use-better-icon-fonts/  
-->
<?php include_once("public/img/hex_social_media_icons.svg"); ?>
<div id="social__container" class="center">
  <a href="https://github.com/Zamerick">
    <svg viewBox="0 0 125 125" class="social__icon">
      <use xlink:href="#git"></use>
    </svg>
  </a>        
  <a href="http://www.linkedin.com/in/alexoxthorn">
    <svg viewBox="0 0 125 125" class="social__icon">
      <use xlink:href="#linkedin"></use>
    </svg>
  </a>
  <a href="https://www.twitter.com/Zamerick">
    <svg viewBox="0 0 125 125" class="social__icon">
      <use xlink:href="#twitter"></use>
    </svg>
  </a>
  <a href="https://bitbucket.org/zamerick/">
    <svg viewBox="0 0 125 125" class="social__icon">
      <use xlink:href="#bitbucket"></use>
    </svg>
  </a>
</div>