USE portfolio;
CREATE TABLE content(
   id          INT             NOT NULL AUTO_INCREMENT,
   description VARCHAR (255)   NOT NULL,
   category    VARCHAR (255)   NOT NULL,
   timestamp   DATETIME        NOT NULL DEFAULT CURRENT_TIMESTAMP,
   PRIMARY KEY (ID)
);