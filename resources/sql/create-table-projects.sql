USE portfolio;

CREATE TABLE projects(
   ID          INT             NOT NULL AUTO_INCREMENT,
   NAME        VARCHAR (255)   NOT NULL,
   URL         VARCHAR (255)   NOT NUll,
   IMG         VARCHAR (255)   NOT NUll,
   DESCRIPTION VARCHAR (255)   NOT NULL,
   CATEGORY    VARCHAR (255)   NOT NULL,
   PRIMARY KEY (ID)
);
