window.$ = window.jQuery = require('jquery');
require('jquery.scrollto');
function onHover()
{
    $(this).children('img').attr('src', $(this).children('img').attr('src').replace(/\.png/, '-on.png') );

    $(this).children('div').toggleClass("project__overlay_show animate fadeInUp");
}
function offHover()
{
    $(this).children('img').attr('src', $(this).children('img').attr('src').replace(/\-on.png/, '.png') );
    $(this).children('div').toggleClass("project__overlay_show animate fadeInUp");
}
$(document).ready(function() {
        $("a#goToProjects").click(function() { $(window).scrollTo('#projects', 800); })
        $("a#goToSkills").click(function()   { $(window).scrollTo('#skills',   800); });
        $("a#goToContact").click(function()  { $(window).scrollTo('#contact',  800); });
        $('.project').hover(onHover, offHover);
    });